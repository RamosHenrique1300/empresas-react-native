![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações necessárias para a correção do projeto Empresas.

---

### Bibliotecas Usadas

- React-Navigation para navegação entre telas.
- React-Redux e Redux para compartilhamento de estados entre telas apesar de ser usado também o React Context no aplicativo.
- Axios para facilitar na hora de fazer requisiçoes e nao precisar ficar usando e configurando o `fetch` toda hora
- @react-native-community/async-storage para gerenciar e guardar informações no celular da pessoa

### Como Executar

- Entre na pasta `App` e lá dentro execute `react-native run-android` ou `react-native run-ios`

### Sugestões

- Quando eu recebia dado de uma empresa ou do usuario de login, eu tinha no JSON uma key chamada `photo`, porem eu tentei diferentes tipos de URLs para achar a foto e não consegui por isso deixei no codigo comentado como eu faria caso a foto funcionasse e substitui essas fotos por fotos que podem ser achadas na internet. #AnimaisFofos
