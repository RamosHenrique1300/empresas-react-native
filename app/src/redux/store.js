import {createStore, combineReducers} from 'redux';
import userReducer from './reducers/userReducer';
import tokenReducer from './reducers/tokenReducer';

export default createStore(
  combineReducers({
    user: userReducer,
    token: tokenReducer,
  }),
);
