export default {
  set(data) {
    return {type: 'SET_USER', payload: data};
  },
};
