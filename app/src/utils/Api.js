import axios from 'axios';

var production = 'https://empresas.ioasys.com.br/api/v1';

const api = axios.create({
  baseURL: production,
});

export default api;
