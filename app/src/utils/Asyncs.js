import AsyncStorage from '@react-native-community/async-storage';

export const User = {
  set: data =>
    new Promise(async (res, rej) => {
      try {
        data = JSON.stringify(data);
        await AsyncStorage.setItem('user', data);
        res(true);
      } catch (err) {
        rej(`set user ${err}`);
      }
    }),
  get: () =>
    new Promise(async (res, rej) => {
      try {
        const data = await AsyncStorage.getItem('user');
        if (!data) return res(false);
        res(JSON.parse(data));
      } catch (err) {
        rej(`get user ${err}`);
      }
    }),
  remove: () =>
    new Promise(async (res, rej) => {
      try {
        await AsyncStorage.removeItem('user');
        res();
      } catch (err) {
        rej(`remove user ${err}`);
      }
    }),
};

export const Token = {
  set: data =>
    new Promise(async (res, rej) => {
      try {
        await AsyncStorage.setItem('token', JSON.stringify(data));
        res(true);
      } catch (err) {
        rej(`set token ${err}`);
      }
    }),
  get: () =>
    new Promise(async (res, rej) => {
      try {
        const data = await AsyncStorage.getItem('token');
        if (!data) return res(false);
        res(JSON.parse(data));
      } catch (err) {
        rej(`get token ${err}`);
      }
    }),
  remove: () =>
    new Promise(async (res, rej) => {
      try {
        await AsyncStorage.removeItem('token');
        res();
      } catch (err) {
        rej(`remove token ${err}`);
      }
    }),
};
