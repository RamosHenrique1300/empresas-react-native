import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  cover: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
