import React from 'react';
import {View, Image} from 'react-native';
import styles from './styles';
import logo from '../../assets/images/logo_ioasys.png';

const Layout = () => (
  <View style={styles.cover}>
    <Image source={logo} />
  </View>
);

export default Layout;
