import React, {useEffect, useState} from 'react';
import Layout from './layout';
import {useSelector} from 'react-redux';
import api from '../../utils/Api';

const Home = props => {
  const [enterprises, setEnterprises] = useState([]);
  const [filter, setFilter] = useState([]);

  const tokenReducer = useSelector(state => state.token);

  useEffect(() => {
    api
      .get('/enterprises', {headers: tokenReducer})
      .then(async res => {
        const {
          data: {enterprises},
        } = res;

        setEnterprises(enterprises);
      })
      .catch(err => {
        console.warn(err);
      });
  }, []);

  useEffect(() => {
    if (filter !== null) {
      api
        .get(filter !== '' ? `/enterprises?name=${filter}` : '/enterprises', {
          headers: tokenReducer,
        })
        .then(async res => {
          const {
            data: {enterprises},
          } = res;
          console.warn(enterprises);
          setEnterprises(enterprises);
        })
        .catch(err => {
          console.warn(err);
        });
    }
  }, [filter]);

  return (
    <Layout
      enterprises={enterprises}
      filter={filter}
      setFilter={e => setFilter(e)}
    />
  );
};

export default Home;
