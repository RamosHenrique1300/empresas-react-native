import {StyleSheet, Dimensions} from 'react-native';
import {Mixins, Colors, Typography} from '../../styles';

export default (styles = StyleSheet.create({
  cover: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  search: {
    width: '100%',
    ...Mixins.boxShadow(1),
    height: 70,
    ...Mixins.padding(18, 0, 9, 0),
    backgroundColor: Colors.WHITE,
    alignItems: 'center',
    flexDirection: 'column',
  },
  image: {
    width: '100%',
    height: '30%',
  },
  coverTitle: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    ...Typography.FONT_BOLD,
    fontSize: Mixins.scaleFont(26),
    color: Colors.BLACK,
    ...Mixins.margin(0, 0, 16, 0),
  },
  country: {height: 30, width: 40},
  titleSection: {
    ...Typography.FONT_REGULAR,
    fontSize: Mixins.scaleFont(18),
    color: Colors.BLACK,
    ...Mixins.margin(0, 0, 8, 0),
  },
  coverRow: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    ...Typography.FONT_REGULAR,
    fontSize: Mixins.scaleFont(18),
    color: Colors.BLACK,
    ...Mixins.margin(0, 0, 8, 0),
  },
}));
