import React, {useState} from 'react';
import {
  View,
  FlatList,
  Modal,
  Text,
  Image,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import EnterpriseCard from '../../components/molecules/EnterpriseCard';
import Input from '../../components/atoms/Input';
import {Mixins, Colors} from '../../styles';
import api from '../../utils/Api';
import {useSelector} from 'react-redux';

const Layout = props => {
  const [modalVisible, setModalVisible] = useState(false);
  const [enterprise, setEnterprise] = useState(null);

  const tokenReducer = useSelector(state => state.token);

  const getInfosEnterprise = id => {
    setModalVisible(true);
    api
      .get(`/enterprises/${id}`, {headers: tokenReducer})
      .then(async res => {
        const {
          data: {enterprise},
        } = res;

        setEnterprise(enterprise);

        console.warn(enterprise);
      })
      .catch(err => {
        console.warn(err);
      });
  };

  return (
    <View style={styles.cover}>
      <View style={styles.search}>
        <Input
          placeholder={'Filtre aqui...'}
          title={'Buscar'}
          titleStyle={{fontSize: Mixins.scaleFont(16)}}
          value={props.filter}
          inputStyle={{borderBottomWidth: 1}}
          onChangeText={e => props.setFilter(e)}
        />
      </View>
      {props.enterprises.length > 0 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={props.enterprises}
          renderItem={({item}) => (
            <EnterpriseCard
              data={item}
              onPress={() => getInfosEnterprise(item?.id)}
            />
          )}
          keyExtractor={item => item.id}
        />
      ) : (
        <ActivityIndicator
          color={Colors.BLACK}
          size={'large'}
          style={{marginTop: 80}}
        />
      )}

      <Modal
        animationType="slide"
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
          setEnterprise(null);
        }}>
        <View
          style={{
            width: '100%',
            height: '100%',
            alignItems: 'center',
            flexDirection: 'column',
          }}>
          <Image
            style={styles.image}
            source={
              enterprise?.photo === null
                ? {
                    uri:
                      'https://www.rd.com/wp-content/uploads/2018/12/50-Funny-Animal-Pictures-That-You-Need-In-Your-Life-4.jpg',
                  }
                : {
                    //uri: `https://empresas.ioasys.com.br/api/v1${enterprise.photo}`,
                    uri:
                      'https://www.imaginacaofertil.com.br/wp-content/uploads/2013/03/sad-dog.jpeg',
                  }
            }
          />
          <View
            style={{
              width: '100%',
              height: '70%',
              alignItems: 'center',
              flexDirection: 'column',
              ...Mixins.padding(16),
              justifyContent: 'space-around',
            }}>
            <View style={styles.coverTitle}>
              <Text style={styles.title}>{enterprise?.enterprise_name}</Text>
              <Image
                style={styles.country}
                source={{
                  uri: `https://www.sogeografia.com.br/figuras/bandeiras/america/${enterprise?.country.toLowerCase()}.jpg`,
                }}
              />
            </View>
            <View style={styles.coverRow}>
              <Text style={styles.titleSectionRow}>Share Price:</Text>
              <Text style={[styles.text, {...Mixins.margin(0, 0, 0, 4)}]}>
                {enterprise?.share_price}
              </Text>
            </View>
            <View style={styles.coverRow}>
              <Text style={styles.titleSectionRow}>City:</Text>
              <Text style={[styles.text, {...Mixins.margin(0, 0, 0, 4)}]}>
                {enterprise?.city}
              </Text>
            </View>
            <View style={styles.coverRow}>
              <Text style={styles.titleSectionRow}>Type:</Text>
              <Text style={[styles.text, {...Mixins.margin(0, 0, 0, 4)}]}>
                {enterprise?.enterprise_type?.enterprise_type_name}
              </Text>
            </View>
            <View>
              <Text style={styles.titleSection}>Description:</Text>
              <Text style={styles.text}>{enterprise?.description}</Text>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default React.memo(Layout);
