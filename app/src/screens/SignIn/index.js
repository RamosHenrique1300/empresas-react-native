import React, {useState} from 'react';
import AuthContext from '../../utils/AuthContext';
import Layout from './layout';

const SignIn = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [invalid, setInvalid] = useState(false);

  const {signIn} = React.useContext(AuthContext);

  const makeSignIn = () => {
    if (email.trim() === '' && password.trim() === '') {
      setInvalid(true);
    } else {
      signIn({email, password}).catch(setInvalid(true));
    }
  };

  return (
    <Layout
      email={email}
      password={password}
      invalid={invalid}
      setEmail={e => setEmail(e)}
      setPassword={e => setPassword(e)}
      login={() => makeSignIn()}
    />
  );
};

export default SignIn;
