import {StyleSheet} from 'react-native';
import {Mixins, Colors, Typography} from '../../styles';

export default (styles = StyleSheet.create({
  cover: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    ...Mixins.padding(16, 16, 0, 16),
  },
  textSignUp: {
    flexDirection: 'row',
    alignItems: 'center',
    ...Mixins.margin(20, 0, 0, 0),
  },
  errorText: {
    ...Mixins.margin(0, 0, 20, 0),
    color: Colors.ALERT,
    ...Typography.FONT_BOLD,
  },
}));
