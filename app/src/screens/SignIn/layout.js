import React from 'react';
import {View, Image, Text} from 'react-native';
import styles from './styles';
import TextFieldIn from '../../components/atoms/Input';
import Button from '../../components/atoms/Button';

import logo from '../../assets/images/logo_ioasys.png';

const Layout = props => {
  return (
    <View style={styles.cover}>
      <Image source={logo} />
      <TextFieldIn
        title={'Email'}
        value={props.email}
        onChangeText={e => props.setEmail(e)}
        placeholder={'email@domain.com'}
      />
      <TextFieldIn
        title={'Senha'}
        password
        value={props.password}
        onChangeText={e => props.setPassword(e)}
        placeholder={'password'}
      />
      {props.invalid ? (
        <Text style={styles.errorText}>Dados inválidos. Tente novamente!</Text>
      ) : (
        <></>
      )}
      <Button title={'ENTRAR'} onPress={() => props.login()} />
    </View>
  );
};

export default Layout;
