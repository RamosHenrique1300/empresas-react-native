import React, {useReducer, useMemo, useEffect} from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

import AuthContext from './utils/AuthContext';
import api from './utils/Api';
import {User, Token} from './utils/Asyncs';
import userAction from './redux/actions/userAction';
import tokenAction from './redux/actions/tokenAction';

import {useDispatch} from 'react-redux';

import Home from './screens/Home';
import SignIn from './screens/SignIn';
import Splash from './screens/Splash';
import {Colors} from './styles';

import Icon from './assets/images/logout.png';

const Stack = createStackNavigator();

const Routes = ({navigation}) => {
  const reduxDispatch = useDispatch();
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    },
  );

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      Token.get()
        .then(token => {
          if (token) {
            reduxDispatch(tokenAction.set(token));

            User.get()
              .then(user => {
                if (user) {
                  reduxDispatch(userAction.set(user));
                  dispatch({type: 'RESTORE_TOKEN', token});
                } else {
                  dispatch({type: 'RESTORE_TOKEN', user: null});
                }
              })
              .catch(e => {
                console.warn('NAO PEGOU TOKEN');
              });
          } else {
            dispatch({type: 'RESTORE_TOKEN', token: null});
          }
        })
        .catch(e => {
          console.warn('NAO PEGOU TOKEN');
        });
    };

    bootstrapAsync();
  }, []);

  const authContext = useMemo(
    () => ({
      signIn: async data => {
        api
          .post('/users/auth/sign_in', {...data})
          .then(async res => {
            const {data: user, headers: token} = res;
            const filtredToken = {
              'access-token': token['access-token'],
              client: token.client,
              uid: token.uid,
            };
            Token.set(filtredToken).then(() => {
              User.set(user).then(() => {
                reduxDispatch(tokenAction.set(filtredToken));
                reduxDispatch(userAction.set({...user, token: null}));
                dispatch({type: 'SIGN_IN', token: filtredToken});
              });
            });
          })
          .catch(err => alert(err));
      },
      signOut: async () => {
        Token.remove();
        User.remove();
        reduxDispatch(tokenAction.set(null));
        reduxDispatch(userAction.set(null));
        dispatch({type: 'SIGN_OUT', token: null});
      },
    }),
    [],
  );

  return (
    <NavigationContainer>
      <AuthContext.Provider value={authContext}>
        <Stack.Navigator initialRouteName="Splash">
          {state.isLoading ? (
            <Stack.Screen
              options={{
                header: () => null,
              }}
              name="Splash"
              component={Splash}
            />
          ) : state.userToken === null ? (
            <Stack.Screen
              name="SignIn"
              component={SignIn}
              options={{
                header: () => null,
                animationTypeForReplace: state.isSignout ? 'pop' : 'push',
              }}
            />
          ) : (
            <>
              <Stack.Screen
                name="Home"
                component={Home}
                options={{
                  title: 'Empresas',
                  headerStyle: {
                    backgroundColor: Colors.BLACK,
                  },
                  headerTitleStyle: {
                    fontWeight: 'bold',
                    color: Colors.WHITE_87,
                  },
                  headerRight: () => (
                    <TouchableOpacity
                      style={{backgroundColor: 'transparent'}}
                      onPress={() => authContext.signOut()}>
                      <Image
                        source={require('./assets/images/logout.png')}
                        style={{width: 30, height: 30}}
                      />
                    </TouchableOpacity>
                  ),
                }}
              />
            </>
          )}
        </Stack.Navigator>
      </AuthContext.Provider>
    </NavigationContainer>
  );
};

export default Routes;
