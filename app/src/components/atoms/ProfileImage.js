import React from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ProfileImage = props => {
  let width = 39;
  let height = 39;

  props.medium && ((width = 65), (height = 65));
  props.large && ((width = 83), (height = 83));

  return (
    <View
      style={[
        styles.cover,
        {height: height, width: width, borderRadius: width, ...props.styles},
      ]}>
      <Image
        style={[
          styles.image,
          props.imageStyles,
          {width: width, height: height},
        ]}
        source={{
          uri:
            props.uri ||
            'https://m2bob-forum.net/wcf/images/avatars/3e/2720-3e546be0b0701e0cb670fa2f4fcb053d4f7e1ba5.jpg',
        }}
      />
      {props.editable && (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => props.editPress()}
          style={styles.edit}>
          <Icon name={'ios-camera'} size={24} color={'#FFFFFF'} />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  cover: {
    position: 'relative',
  },
  image: {
    borderRadius: 100,
  },
  edit: {
    height: 32,
    width: 32,
    borderRadius: 32,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: -8,
    right: -8,
    backgroundColor: '#9B9B9B',
  },
});

export default ProfileImage;
