import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {Mixins, Colors, Typography} from '../../styles';

const Button = props => {
  return (
    <TouchableOpacity
      style={[styles.cover, props.style]}
      activeOpacity={0.6}
      onPress={() => props.onPress()}>
      {props.loading ? (
        <ActivityIndicator color={Colors.WHITE} size="large" />
      ) : (
        <Text style={[styles.text, props.textStyle]}>{props.title}</Text>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cover: {
    height: 50,
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    width: 220,
    borderRadius: 30,
    ...Mixins.boxShadow(3),
    backgroundColor: Colors.BLACK,
  },
  text: {
    backgroundColor: 'transparent',
    ...Typography.FONT_BOLD,
    fontSize: Mixins.scaleFont(18),
    color: Colors.WHITE_87,
  },
});

export default Button;
