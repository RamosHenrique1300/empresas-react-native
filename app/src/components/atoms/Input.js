import React from 'react';
import {StyleSheet, TextInput, View, Text} from 'react-native';
import {Mixins, Colors, Typography} from '../../styles';

const Input = props => {
  return (
    <View style={styles.cover}>
      {props.title ? (
        <Text style={[styles.text, props.titleStyle]}>{props.title}</Text>
      ) : null}
      <TextInput
        secureTextEntry={props.password}
        style={[styles.input, props.inputStyle]}
        placeholder={props.placeholder}
        onChangeText={e => props.onChangeText(e)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  cover: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'center',
    width: '80%',
    ...Mixins.margin(0, 0, 30, 0),
  },
  input: {
    borderBottomColor: Colors.BLACK,
    borderBottomWidth: 2,
    width: '100%',
    padding: 5,
  },
  text: {
    backgroundColor: 'transparent',
    ...Typography.FONT_REGULAR,
    fontSize: Mixins.scaleFont(18),
    color: Colors.BLACK,
  },
});

export default Input;
