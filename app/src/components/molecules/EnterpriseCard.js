import React from 'react';
import {TouchableOpacity, Text, StyleSheet, View, Image} from 'react-native';
import {Mixins, Colors, Typography} from '../../styles';

const EnterpriseCard = props => {
  return (
    <TouchableOpacity
      style={styles.cover}
      activeOpacity={0.6}
      onPress={() => props.onPress()}>
      <Image
        style={styles.image}
        source={
          props.data.photo === null
            ? {
                uri:
                  'https://www.rd.com/wp-content/uploads/2018/12/50-Funny-Animal-Pictures-That-You-Need-In-Your-Life-4.jpg',
              }
            : {
                //uri: `https://empresas.ioasys.com.br/api/v1${props.data.photo}`,
                uri:
                  'https://www.imaginacaofertil.com.br/wp-content/uploads/2013/03/sad-dog.jpeg',
              }
        }
      />
      <View style={styles.infos}>
        <View style={styles.coverTitle}>
          <Text style={styles.title}>{props.data.enterprise_name}</Text>
          <Image
            style={styles.country}
            source={{
              uri: `https://www.sogeografia.com.br/figuras/bandeiras/america/${props.data.country.toLowerCase()}.jpg`,
            }}
          />
        </View>
        <Text numberOfLines={3} style={styles.text}>
          {props.data.description}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cover: {
    height: 300,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '94%',
    borderRadius: 5,
    ...Mixins.boxShadow(1),
    backgroundColor: Colors.WHITE,
    ...Mixins.margin(10, 0, 10, 0),

    alignSelf: 'center',
  },
  infos: {
    justifyContent: 'space-around',
    ...Mixins.padding(16),
    height: 150,
    borderTopColor: Colors.BLACK,
    borderTopWidth: 0.5,
  },
  coverTitle: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    alignItems: 'center',
  },
  title: {
    ...Typography.FONT_BOLD,
    fontSize: Mixins.scaleFont(18),
    color: Colors.BLACK,
    ...Mixins.margin(0, 8, 0, 0),
  },
  text: {
    ...Typography.FONT_REGULAR,
    fontSize: Mixins.scaleFont(14),
    color: Colors.BLACK,
  },
  image: {
    width: '100%',
    height: 150,
  },
  country: {height: 15, width: 20},
});

export default EnterpriseCard;
