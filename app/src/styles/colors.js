export const PRIMARY = '#00A5D6';
export const SECONDARY = '#006F91';
export const WHITE = 'rgba(255,255,255,1)';
export const WHITE_87 = 'rgba(255,255,255,.87)';
export const WHITE_60 = 'rgba(255,255,255,.87)';
export const WHITE_38 = 'rgba(255,255,255,.87)';
export const BLACK = '#000000';

// ACTIONS
export const SUCCESS = '#3adb76';
export const WARNING = '#ffae00';
export const ALERT = '#cc4b37';

// GRAYSCALE
export const GRAY_1 = '#212121';
export const GRAY_2 = '#303030';
export const GRAY_3 = '#424242';
