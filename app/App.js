import React from 'react';
import {StatusBar} from 'react-native';
import AppNavigation from './src/routes';
import {Provider} from 'react-redux';
import Store from './src/redux/store';
import {Colors} from './src/styles';

const App = () => (
  <Provider store={Store}>
    <StatusBar translucent={true} backgroundColor={Colors.BLACK} />
    <AppNavigation />
  </Provider>
);

export default App;
